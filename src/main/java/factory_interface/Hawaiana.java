package factory_interface;

/**
 * Created by Gustavo on 23/04/2018.
 */
public class Hawaiana implements IPizza{

    @Override
    public void cocer() {
        System.out.print("Cocinando hawaiana");
    }

    @Override
    public void preparar() {
        System.out.print("Preparando hawaiana");
    }

}
