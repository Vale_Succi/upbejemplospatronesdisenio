package factory_interface;

/**
 * Created by Gustavo on 23/04/2018.
 */
public interface IPizza {

    public void cocer();
    public void preparar();

}
