/**
 * Created by Gustavo on 18/04/2018.
 */
public class Logger {

    private static Logger ourInstance = null;
    private String allLog = "";

    private Logger() {

    }

    public static Logger getInstance() {
        if (ourInstance == null)
            ourInstance = new Logger();
        return ourInstance;
    }

    public void writeLog(String log) {
        allLog = allLog + log + "\n";
    }
    public String getLog() {
        return allLog;
    }

    public void destruir(){

        ourInstance=null;
    }

}
