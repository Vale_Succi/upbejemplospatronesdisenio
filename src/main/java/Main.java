/**
 * Created by Gustavo on 18/04/2018.
 */
public class Main {

    public static void main(String[] args) {

        Persona p = new Persona("Juan", 2000, "Boliviano");
        p.getEdad();

        Figura f = new Figura("Cuadrado", 4, "Cuadrado bonito negro");
        f.getDescripcion();

        System.out.println(Logger.getInstance().getLog());
    }
}
