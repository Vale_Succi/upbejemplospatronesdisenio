/**
 * Created by Gustavo on 18/04/2018.
 */
public class Figura {

    private String nombre;
    private int nLados;
    private String descripcion;

    public Figura(String nombre, int nLados, String descripcion) {
        this.nombre = nombre;
        this.nLados = nLados;
        this.descripcion = descripcion;
        Logger.getInstance().writeLog("Figura creada");
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getnLados() {
        return nLados;
    }

    public void setnLados(int nLados) {
        this.nLados = nLados;
    }

    public String getDescripcion() {
        Logger.getInstance().writeLog("Pedir descripcion");
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
