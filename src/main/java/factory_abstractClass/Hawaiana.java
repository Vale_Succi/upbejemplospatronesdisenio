package factory_abstractClass;

/**
 * Created by Gustavo on 23/04/2018.
 */
public class Hawaiana extends Pizza {

    @Override
    public void servir() {
        System.out.print("Sirviendo hawaiana");
    }
}
