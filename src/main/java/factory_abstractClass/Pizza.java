package factory_abstractClass;

/**
 * Created by Gustavo on 23/04/2018.
 */
public abstract class Pizza {

    public void cocer(){
        System.out.println("Cocinando pizza");
    }

    public void preparar(){
        System.out.println("Preparando pizza");
    }

    public abstract void servir();

}
