/**
 * Created by Gustavo on 18/04/2018.
 */
public class Persona {

    private String nombre;
    private int anioNacimiento;
    private String nacionalidad;

    public Persona(String nombre, int anioNacimiento, String nacionalidad) {
        this.nombre = nombre;
        this.anioNacimiento = anioNacimiento;
        this.nacionalidad = nacionalidad;
        Logger.getInstance().writeLog("Persona creada");
    }

    public String getNombre() {
        return nombre;
    }

    public int getAnioNacimiento() {
        return anioNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAnioNacimiento(int anioNacimiento) {
        this.anioNacimiento = anioNacimiento;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public int getEdad(){
        Logger.getInstance().writeLog("Calculando edad");
        return 2018 - anioNacimiento;
    }

}
